﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MusicGroups.Managers;
using MusicGroups.Models.Entities.Common;

namespace MusicGroups.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GroupsController : ControllerBase
    {
        private IGroupManager groupManager;

        public GroupsController(IGroupManager groupManager)
        {
            this.groupManager = groupManager;
        }

        [HttpPost("AddGroup")]
        public Group AddGroup([FromBody]string name)
        {
            Debug.WriteLine("Name: " + name);
            var group = new Group();
            group.Name = name;
            if (group.Name == null)
            {
                ModelState.AddModelError("group.Name", "error");
            }
            else
            {
                groupManager.AddGroup(group);
            }
            return group;
        }

        [HttpGet("GetGroups")]
        public IEnumerable<Group> GetGroups()
        {
            return groupManager.GetAllGroups();
        }

        [HttpDelete("RemoveGroup/{id}")]
        public void RemoveGroup(Guid id)
        {
            Debug.WriteLine("id for remove: " + id);
            groupManager.RemoveGroup(id);
        }

        [HttpGet("GetGroup/{id}")]
        public Group GetGroup(Guid id)
        {
            return groupManager.GetGroupById(id);
        }

        
        [HttpGet("SearchGroups/{name}")]
        public IEnumerable<Group> SearchGroups(string name)
        {
            return groupManager.SearchGroups(name);
        }


        [HttpPut("UpdateGroup/{id}")]
        public Group UpdateGroup(Guid id, [FromBody]string name)
        {
            Debug.WriteLine("Id: " + id);
            Debug.WriteLine("Name: " + name);
            var oldGroup = groupManager.GetGroupById(id);
            var newGroup = new Group();
            newGroup.Id = id;
            newGroup.Name = name;
            if (newGroup.Name == null)
            {
                ModelState.AddModelError("group.Name", "error");
            }
            else
            {
                groupManager.UpdateGroup(oldGroup, newGroup);
            }
            return newGroup;
        }
    }
}

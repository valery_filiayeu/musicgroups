﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using MusicGroups.Managers;
using MusicGroups.Models.Entities.Common;

namespace MusicGroups.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AlbumsController : ControllerBase
    {
        private IAlbumManager albumManager;

        public AlbumsController(IAlbumManager albumManager)
        {
            this.albumManager = albumManager;
        }

        [HttpGet("GetAlbums/{id}")]
        public IEnumerable<Album> GetAlbums(Guid id)
        {
            Debug.WriteLine("id:" + id);
            return albumManager.GetAlbums(id);
        }

        [HttpPost("AddAlbum")]
        public Album AddAlbum([FromBody]Album album)
        {
            Debug.WriteLine("Name: " + album.Name);
            Debug.WriteLine("Name: " + album.GroupId);
            //var album = new Album();
            if (album.Name == null)
            {
                ModelState.AddModelError("album.Name", "error");
            }
            else
            {
                albumManager.AddAlbum(album, album.GroupId);
            }
            return album;
        }

        [HttpDelete("RemoveAlbum/{id}")]
        public void RemoveAlbum(Guid id)
        {
            Debug.WriteLine("id for remove: " + id);
            albumManager.RemoveAlbum(id);
        }


        [HttpGet("GetAlbum/{id}")]
        public Album GetAlbum(Guid id)
        {
            return albumManager.GetAlbumById(id);
        }

        [HttpPut("UpdateAlbum/{id}")]
        public Album UpdateAlbum(Guid id, [FromBody]string name)
        {
            Debug.WriteLine("Id: " + id);
            Debug.WriteLine("Name: " + name);
            var newAlbum = new Album();
            var oldAlbum = albumManager.GetAlbumById(id);
            newAlbum.Id = id;
            newAlbum.Name = name;
            newAlbum.GroupId = oldAlbum.GroupId;
            if (name == null)
            {
                ModelState.AddModelError("album.Name", "error");
            }
            else
            {
                albumManager.UpdateAlbum(oldAlbum, newAlbum);
            }
            return newAlbum;
        }

    }
}

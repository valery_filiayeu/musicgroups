﻿using System;
using System.Linq;

namespace MusicGroups.Utils
{
    public class DamerauLevenshteinDistance
    {
        private const double CoefMaxDistance = 0.3;

        public int GetDistance(string s, string t)
        {
            int[,] d = new int[s.Length + 1, t.Length + 1];

            for (int i = 0; i < s.Length + 1; i++)
            {
                d[i, 0] = i;
            }

            for (int j = 1; j < t.Length + 1; j++)
            {
                d[0, j] = j;
            }

            for (int i = 1; i <= s.Length; i++)
            {
                for (int j = 1; j <= t.Length; j++)
                {
                    d[i, j] = (new int[]
                    {
                        //delete
                        d[i - 1, j] + 1,    
                        //insert
                        d[i, j - 1] + 1,    
                        //substitution
                        d[i - 1, j - 1] + (s[i - 1] != t[j - 1] ? 1 : 0)
                    }).Min();
                    if ((i > 1) && (j > 1) && (s[i - 1] == t[j - 2]) && (s[i - 2] == t[j - 1]))
                    {
                        d[i, j] = (new int[]
                        {
                            //transposition
                            d[i - 2, j - 2] + 1
                        }).Min();
                    }
                }
            }
            return d[s.Length, t.Length];
        }

        public int GetMaxDistance(string source)
        {
            return (int)Math.Round(CoefMaxDistance * source.Length);
        }
    }
}

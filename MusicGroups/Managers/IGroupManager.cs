﻿using MusicGroups.Models.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicGroups.Managers
{
    public interface IGroupManager
    {
        void AddGroup(Group group);
        IList<Group> GetAllGroups();
        void RemoveGroup(Guid id);
        Group GetGroupById(Guid id);
        void UpdateGroup(Group group, Group newGroup);
        IList<Group> SearchGroups(string name);
    }
}

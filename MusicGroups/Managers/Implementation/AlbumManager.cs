﻿using System;
using System.Collections.Generic;
using System.Linq;
using MusicGroups.Models.Entities.Common;
using MusicGroups.Repository;

namespace MusicGroups.Managers.Implementation
{
    public class AlbumManager : IAlbumManager
    {
        private IUnitOfWork unitOfWork;
        private IRepository<Guid, Album> albumRepository;

        public AlbumManager(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            albumRepository = this.unitOfWork.GetRepository<Guid, Album>();
        }

        public void AddAlbum(Album album, Guid groupId)
        {
            album.Id = Guid.NewGuid();
            album.GroupId = groupId;
            albumRepository.Add(album);
            unitOfWork.Commit();
        }

        public Album GetAlbumById(Guid id)
        {
            var album = albumRepository.GetAllAsQueryable()
                .Single(x => x.Id.Equals(id));
            return album;

        }

        public IList<Album> GetAlbums(Guid groupId)
        {
            var albums = albumRepository.GetAllAsQueryable()
                .Where(x => x.GroupId.Equals(groupId)).ToList();
            return albums;
        }

        public void RemoveAlbum(Guid id)
        {
            albumRepository.Remove(GetAlbumById(id));
            unitOfWork.Commit();
        }

        public void UpdateAlbum(Album album, Album newAlbum)
        {
            album.Id = newAlbum.Id;
            album.Name = newAlbum.Name;
            album.GroupId = newAlbum.GroupId;
            albumRepository.Update(album);
            unitOfWork.Commit();
        }
    }
}

﻿using MusicGroups.Models.Entities.Common;
using MusicGroups.Repository;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;
using MusicGroups.Utils;

namespace MusicGroups.Managers.Implementation
{
    public class GroupManager : IGroupManager
    {
        private IUnitOfWork unitOfWork;
        private IRepository<Guid, Group> groupRepository;

        //IList<Group> _allGroups;

        public GroupManager(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            groupRepository = this.unitOfWork.GetRepository<Guid, Group>();
            //_allGroups = new List<Group>();
        }

        public void AddGroup(Group group)
        {
            group.Id = Guid.NewGuid();
            groupRepository.Add(group);
            unitOfWork.Commit();
        }

        public IList<Group> SearchGroups(string name)
        {
            var distance = new DamerauLevenshteinDistance();
            var foundedGroups = new List<Group>();
            var dist = 0;
            foundedGroups.Clear();

            foreach (var group in GetAllGroups())
            {
                dist = distance.GetDistance(name, group.Name);
                if (dist <= distance.GetMaxDistance(group.Name))
                {
                    foundedGroups.Add(group);
                }
            }
            return foundedGroups;
        }

        public IList<Group> GetAllGroups()
        {
            var allGroups = groupRepository.GetAllAsQueryable().ToList();
            //foreach (var group in allGroups)
            //{
            //    Debug.WriteLine("employee: " + group.Name);
            //}
            return allGroups;
        }

        public Group GetGroupById(Guid id)
        {
            var group = groupRepository.GetAllAsQueryable()
                .Single(x => x.Id.Equals(id));
            return group;
        }

        public void RemoveGroup(Guid id)
        {
            groupRepository.Remove(GetGroupById(id));
            unitOfWork.Commit();
        }

        public void UpdateGroup(Group group, Group newGroup)
        {
            group.Id = newGroup.Id;
            group.Name = newGroup.Name;
            groupRepository.Update(group);
            unitOfWork.Commit();
        }
    }
}

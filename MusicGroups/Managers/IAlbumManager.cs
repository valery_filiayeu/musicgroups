﻿using MusicGroups.Models.Entities.Common;
using System;
using System.Collections.Generic;

namespace MusicGroups.Managers
{
    public interface IAlbumManager
    {
        void AddAlbum(Album album, Guid groupId);
        IList<Album> GetAlbums(Guid groupId);
        void RemoveAlbum(Guid id);
        Album GetAlbumById(Guid id);
        void UpdateAlbum(Album album, Album newAlbum);
    }
}

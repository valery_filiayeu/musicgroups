﻿using Microsoft.EntityFrameworkCore;
using MusicGroups.Context;
using MusicGroups.Models.Configurations.Common;
using MusicGroups.Models.Entities.Common;

namespace MusicGroups.Models.Context.Implementation
{
    public class DatabaseContext : DbContext, IDatabaseContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options)
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new GroupConfiguration());
            modelBuilder.ApplyConfiguration(new AlbumConfiguration());
        }
    }
}

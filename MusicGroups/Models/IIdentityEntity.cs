﻿namespace MusicGroups.Models
{
    public interface IIdentityEntity<T> where T : struct
    {
        T Id { get; set; }
    }
}

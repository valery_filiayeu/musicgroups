﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MusicGroups.Models.Entities.Common;

namespace MusicGroups.Models.Configurations.Common
{
    public class GroupConfiguration : IEntityTypeConfiguration<Group>
    {
        public void Configure(EntityTypeBuilder<Group> builder)
        {
            builder.ToTable("Groups"); //Название таблицы

            builder.HasKey(a => a.Id); //Ключевое поле

            builder.Property(a => a.Id)
                .HasColumnName("Id")
                .ValueGeneratedOnAdd();

            builder.Property(x => x.Name)
                .HasColumnName("Name")
                .IsRequired() //Поле обязательное
                .HasMaxLength(255); //Максимальный размер - 255 символов

            builder.Metadata.FindNavigation(nameof(Group.Albums)).SetPropertyAccessMode(PropertyAccessMode.Field);
            builder.HasMany(x => x.Albums).WithOne(b => b.Group).HasForeignKey(b => b.GroupId)
                .OnDelete(DeleteBehavior.Cascade);
        }


    }
}

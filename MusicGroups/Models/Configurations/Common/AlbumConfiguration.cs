﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MusicGroups.Models.Entities.Common;

namespace MusicGroups.Models.Configurations.Common
{
    public class AlbumConfiguration : IEntityTypeConfiguration<Album>
    {
        public void Configure(EntityTypeBuilder<Album> builder)
        {
            builder.ToTable("Albums"); //Название таблицы

            builder.HasKey(a => a.Id); //Ключевое поле

            builder.Property(a => a.Id)
                .HasColumnName("Id")
                .ValueGeneratedOnAdd();


            builder.Property(x => x.Name)
                .HasColumnName("Name")
                .IsRequired() //Поле обязательное
                .HasMaxLength(255); //Максимальный размер - 255 символов
        }
    }
}

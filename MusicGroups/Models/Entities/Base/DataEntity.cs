﻿using System;

namespace MusicGroups.Models.Entities.Base
{
    public class DataEntity : IIdentityEntity<Guid>
    {
        public Guid Id { get; set; }
    }
}

﻿using MusicGroups.Models.Entities.Base;
using System.Collections.Generic;

namespace MusicGroups.Models.Entities.Common
{
    public class Group : DataEntity
    {
        public string Name { get; set; }
        public IList<Album> Albums { get; set; }

        public Group()
        {
            Albums = new List<Album>();
        }

        public override string ToString()
        {
            return Id + ";" + Name;
        }
    }
}

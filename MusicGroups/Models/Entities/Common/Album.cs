﻿using MusicGroups.Models.Entities.Base;
using System;

namespace MusicGroups.Models.Entities.Common
{
    public class Album : DataEntity
    {
        public string Name { get; set; }
        public Guid GroupId { get; set; }
        public Group Group { get; set; }

        public override string ToString()
        {
            return Id + ";" + Name + ";" + GroupId;
        }
    }
}

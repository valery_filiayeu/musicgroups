function removeGroup(id) {
	$(document).ready(function () {  
		$.ajax({  
            type: "DELETE",  
            url: "https://localhost:44340/api/groups/removegroup/" + id + "/", 
            contentType: "application/json; charset=utf-8",  
			success: function () {
				$("tr[data-row-id='" + id + "']").remove();
			},
		});         
    });  
}

function searchGroups(name) {
	if (name.length) {
		$.ajax({  
			type: "GET",  
			url: "https://localhost:44340/api/groups/searchgroups/" + name + "/", 
			contentType: "application/json; charset=utf-8",  
			dataType: "json",  
			success: function (data) {
				isEmpty = $.isEmptyObject(data);
				
				if (!isEmpty) {
					$('#TableGroups').empty();
					var rows = "";  
					$.each(data, function (i, item) { 
					  rows += rowEditTable(item);          
					}); //End of foreach Loop   
					$('#TableGroups').append(rows);	  
				} else {
					alert("No result");
				}
			  // console.log(data);  
			}, //End of AJAX Success function  
			error: function(xhr, status, error) {
			  	console.log(xhr.responseText + '|\n' + status + '|\n' +error);
			},
		});         
	}
}

$("body").on("click", ".removeLink", function () {
	var name = $(this).data("name");
	if (confirm("Are you sure you want to delete the group " + name)) {
		var id = $(this).data("id");
		console.log(id);		
		removeGroup(id);
	}
})

$("form").submit(function (e) {
	e.preventDefault();	
    var name = this.elements["searchGroup"].value;
	// console.log(name);
	searchGroups(name)
});

$("body").on("click", ".editLink", function () {
    var id = $(this).data("id");
	var name = $(this).data("name");
	window.location.href = 'add_group.html?id=' + id;
})

getAllGroups($('#TableGroups'), rowEditTable);

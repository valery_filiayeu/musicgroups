var groupId;

//the group selected
var select = document.querySelector('select');
select.onchange = function() {
	var indexSelected = select.selectedIndex,
	option = select.querySelectorAll('option')[indexSelected];
	groupId = $(option).data('group-id');
	$('#TableAlbums').empty();
	getAlbumsByGroup(groupId, $('#TableAlbums'), rowEditTable);
}

function removeAlbum(id) {
	$(document).ready(function () {  
		$.ajax({  
            type: "DELETE",  
            url: "https://localhost:44340/api/albums/removealbum/" + id + "/", 
            contentType: "application/json; charset=utf-8",  
			success: function () {
				$("tr[data-row-id='" + id + "']").remove();
			},
		});         
    });  
}

//Button Add Album
$("body").on("click", ".btn", function () {
	if (groupId) {
		console.log(groupId);
		window.location.href = 'add_album.html?groupId=' + groupId;
	} else {
		alert("Choose the group");
	}
})

$("body").on("click", ".removeLink", function () {
	var name = $(this).data("name");
	if (confirm("Are you sure you want to delete the album " + name)) {
		var id = $(this).data("id");
		console.log(id);		
		removeAlbum(id);
	}
})

$("body").on("click", ".editLink", function () {
    var id = $(this).data("id");
	var name = $(this).data("name");
	window.location.href = 'add_album.html?id=' + id;
})

getAllGroups($('#SelectGroup'), rowGroupSelect);

function rowGroupSelect(item) {
	return "<option data-group-id='" + item.id + "'>" + item.name + "</option>";
}  
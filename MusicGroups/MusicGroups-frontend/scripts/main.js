var target;
function expandButtonClick(evt) {
  target = evt.target;
  var isOpen = !target.parentNode.classList.toggle('ExpandClosed');
  if (isOpen) {
    id = $(target).parent().data('group-id');
    elm = $(target).parent().find('.Content.GroupContent')
    getAlbumsByGroup(id, elm, rowAlbumsMain);  
  } else {
    $(target).parent().find('.Name').remove();  
  }      
}

function getAlbumsByGroup(id, elm, row) {
  $.ajax({  
    type: "GET",  
    url: "https://localhost:44340/api/albums/getalbums/"+ id +"/",  
    contentType: "application/json; charset=utf-8",  
    dataType: "json",  
    success: function (data) {  
      var rows = "";
      $.each(data, function (i, item) { 
        rows += row(item);           
      }); //End of foreach Loop   
      elm.append(rows);
    }, //End of AJAX Success function
    error: function(xhr, status, error) {
			console.log(xhr.responseText + '|\n' + status + '|\n' +error);
		},
  });  
}

function getAllGroups(elm, row) {
	$.ajax({  
    type: "GET",  
    url: "https://localhost:44340/api/groups/getgroups", 
    contentType: "application/json; charset=utf-8",  
    dataType: "json",  
    success: function (data) {
      var rows = "";  
      $.each(data, function (i, item) { 
        rows += row(item);          
      }); //End of foreach Loop   
      elm.append(rows);
      $('#ListGroups').find('.Expand.GroupTitle').on('click', expandButtonClick);  
      // console.log(data);  
    }, //End of AJAX Success function  
    error: function(xhr, status, error) {
      console.log(xhr.responseText + '|\n' + status + '|\n' +error);
    },
  });         
}

getAllGroups($('#ListGroups'), rowGroupsMain);

function rowGroupsMain(item) {
	return "<div>" +  
    "<div id='Name'>" + 
      "<div class='Container'>" +
        "<div class='Node IsRoot ExpandClosed' data-group-id='" + item.id + "'>" +
          "<div class='Expand GroupTitle'></div>" +
          "<div class='Content GroupContent'>" + item.name + "</div>" +
        "</div>" +
      "</div>" +
    "</div>" +  
  "</div>";  
}

function rowAlbumsMain(item) {
  return "<div>" +  
    "<div class='Name'>" + 
      "<div class='Container'>" +
        "<div class='Node ExpandLeaf IsLast'>" +
          "<div class='Expand'></div>" +
          "<div class='Content'>" + item.name + "</div>" +
        "</div>" +
      "</div>" +
    "</div>" +  
  "</div>";
}

function rowEditTable(item) {
	return "<tr data-row-id='" + item.id + "'>" +
	  "<td>" + item.name + "</td>" + 			
	  "<td><a class='editLink' data-id='" + item.id + "' data-name='" + item.name + "'>Edit</a> | " +
		"<a class='removeLink' data-id='" + item.id + "' data-name='" + item.name + "'>Delete</a>" +
	  "</td>" +
	"</tr>";
}


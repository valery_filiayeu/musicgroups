function reset(form) {
	var form = document.forms[form];
	form.reset();
    form.elements["id"].value = 0;
}
		
function redirect(url) {
    window.location.href = url;
}

// Get args from url
function getArgs() {
	var args = new Object();
	var query = location.search.substring(1);
	var pairs = query.split("&");
    for(var i = 0; i < pairs.length; i++) {
		var pos = pairs[i].indexOf('=');
		if (pos == -1) { 
			continue;
		}
		var argname = pairs[i].substring(0, pos);
		var value = pairs[i].substring(pos + 1);
		value = decodeURIComponent(value);
		args[argname] = value;
    }
    return args;
}
		
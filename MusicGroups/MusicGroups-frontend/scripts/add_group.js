function addGroup(name) {	
	$.ajax({
		url: "https://localhost:44340/api/groups/addgroup/",
        method: "POST",
        data: JSON.stringify(name),  
        contentType: "application/json",  
        dataType: "json",  			
        success: function (data) {
			console.log(data.name);
			redirect("edit_groups.html");
		},
		error: function(xhr, status, error) {
			console.log(xhr.responseText + '|\n' + status + '|\n' +error);
		},
	});		
}

function getGroup() {	
	var id = getArgs()['id'];
	console.log(id);
	$.ajax({
		url: "https://localhost:44340/api/groups/getgroup/" + id + "/", 
        contentType: "application/json; charset=utf-8",  
        dataType: "json",  
        success: function (data) {  
			console.log(data);
			var form = document.forms["groupForm"];
			form.elements["id"].value = id;
			form.elements["name"].value = data.name;
		}, //End of AJAX Success function  
		error: function(xhr, status, error) {
			console.log(xhr.responseText + '|\n' + status + '|\n' +error);
		},
    });        
}

function updateGroup(id, newName) {
	$.ajax({
		url: "https://localhost:44340/api/groups/updategroup/" + id + "/",
        contentType: "application/json",
        method: "PUT",
        data: JSON.stringify(newName),
		dataType: "json",
        success: function (data) {
			console.log(data.name);
			reset("groupForm");
			redirect("edit_groups.html");
        },
			error: function(xhr, status, error) {
			console.log(xhr.responseText + '|\n' + status + '|\n' +error);
		},
    });
}

//Button Add
$("form").submit(function (e) {
	e.preventDefault();	
    var name = this.elements["name"].value;
	var id = this.elements["id"].value;
    
    if (id == 0) {
		console.log(name);
		addGroup(name);
	} else {
        updateGroup(id, name);
	}
	
});
		
$("#ResetGroup").click(function (e) { 
	e.preventDefault();
    reset("groupForm");
	redirect("edit_groups.html");
})

if (getArgs()['id']) {
	console.log("true");
	getGroup();
} 

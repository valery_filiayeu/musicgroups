function addAlbum(name, groupId) {	
	var album = new Object();
	album.name = name;
	album.groupId = groupId;
	console.log(album.name);
	$.ajax({
		url: "https://localhost:44340/api/albums/addalbum/",
        method: "POST",
        data: JSON.stringify(album),  
        contentType: "application/json",  
        dataType: "json",  			
        success: function (data) {
			console.log(data.groupId);
			redirect("edit_albums.html");
		},
		error: function(xhr, status, error) {
			console.log(xhr.responseText + '|\n' + status + '|\n' +error);
		},
	});		
}

function getAlbum() {	
	var id = getArgs()['id'];
	console.log(id);
	$.ajax({
		url: "https://localhost:44340/api/albums/getalbum/" + id + "/", 
        contentType: "application/json; charset=utf-8",  
        dataType: "json",  
        success: function (data) {  
			console.log(data);
			var form = document.forms["albumForm"];
			form.elements["id"].value = id;
			form.elements["name"].value = data.name;
		}, //End of AJAX Success function  
		error: function(xhr, status, error) {
			console.log(xhr.responseText + '|\n' + status + '|\n' +error);
		},
    });        
}

function updateAlbum(id, newName) {
	$.ajax({
		url: "https://localhost:44340/api/albums/updatealbum/" + id + "/",
        contentType: "application/json",
        method: "PUT",
        data: JSON.stringify(newName),
		dataType: "json",
        success: function (data) {
			reset("albumForm");
			redirect("edit_albums.html");
        },
			error: function(xhr, status, error) {
			console.log(xhr.responseText + '|\n' + status + '|\n' +error);
		},
    });
}

// Button Add
$("form").submit(function (e) {
	e.preventDefault();
    var name = this.elements["name"].value;
	var id = this.elements["id"].value;
	var groupId = getArgs()["groupId"];
    console.log(groupId);
    if (id == 0) {
		console.log(name);
		addAlbum(name, groupId);
	} else {
        updateAlbum(id, name);
	}	
});

$("#ResetAlbum").click(function (e) { 
	e.preventDefault();
    reset("albumForm");
	redirect("edit_albums.html");
})


if (getArgs()['id']) {
	console.log("true");
	getAlbum();
} 
		
﻿using MusicGroups.Models;
using System;

namespace MusicGroups.Repository
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<TId, TEntity> GetRepository<TId, TEntity>() where TEntity : class, IIdentityEntity<TId> where TId : struct;
        void Commit(Guid? userAccountId = null);
    }
}

﻿using MusicGroups.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicGroups.Repository
{
    public interface IRepository<TId, TEntity> where TEntity : class, IIdentityEntity<TId> where TId : struct
    {
        TEntity Add(TEntity entity);

        void Remove(TEntity entity);
        void RemoveRange(IEnumerable<TEntity> entities);
        IQueryable<TEntity> GetAllAsQueryable();

        void Update(TEntity entity);
    }
}

﻿using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Infrastructure;
using MusicGroups.Context;
using MusicGroups.Models.Context.Implementation;

namespace MusicGroups.Repository
{
    public interface IContextFactory
    {
        IDatabaseContext CreateDatabaseContext();
    }
}

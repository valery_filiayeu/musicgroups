﻿using Microsoft.EntityFrameworkCore;
using MusicGroups.Context;
using MusicGroups.Models;
using System.Collections.Generic;
using System.Linq;

namespace MusicGroups.Repository.Implementation
{
    public class Repository<TId, TEntity> : IRepository<TId, TEntity> where TEntity : class, IIdentityEntity<TId> where TId : struct
    {
        private readonly IDatabaseContext context;

        public Repository(IDatabaseContext context)
        {
            this.context = context;
        }

        public TEntity Add(TEntity entity)
        {
            return context.Set<TEntity>().Add(entity).Entity;
        }

        public void Remove(TEntity entity)
        {
            context.Set<TEntity>().Remove(entity);
        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            context.Set<TEntity>().RemoveRange(entities);
        }


        public IQueryable<TEntity> GetAllAsQueryable()
        {
            return context.Set<TEntity>();
        }

        public void Update(TEntity entity)
        {
            context.Set<TEntity>().Attach(entity);

            var entry = context.Entry(entity);
            entry.State = EntityState.Modified;
        }

        private void Attach(TEntity entity)
        {
            context.Entry(entity).State = EntityState.Unchanged;
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;
using MusicGroups.Context;
using MusicGroups.Models.Context.Implementation;

namespace MusicGroups.Repository.Implementation
{
    public class ContextFactory : IContextFactory
    {
        private IConfiguration configuration { get; }

        public ContextFactory(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public IDatabaseContext CreateDatabaseContext()
        {
            var optionsBuilder = new DbContextOptionsBuilder<DatabaseContext>();
            optionsBuilder.UseSqlServer(configuration.GetConnectionString("DefaultConnection"));

            // Ensure that the SQLite database and sechema is created!
            var context = new DatabaseContext(optionsBuilder.Options);
            context.Database.EnsureCreated();

            return context;
        }
    }
}

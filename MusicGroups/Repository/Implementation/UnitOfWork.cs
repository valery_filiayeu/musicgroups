﻿using MusicGroups.Context;
using MusicGroups.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace MusicGroups.Repository.Implementation
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IDatabaseContext context;
        private readonly Dictionary<Type, object> repositories;

        public UnitOfWork(IContextFactory contextFactory)
        {
            Debug.WriteLine("UnitOfWork.Create");

            context = contextFactory.CreateDatabaseContext();
            repositories = new Dictionary<Type, object>();
        }

        public void Commit(Guid? id = null)
        {
            Debug.WriteLine("UnitOfWork.Commit");
            context.SaveChanges();
        }

        public void Dispose()
        {
            Debug.WriteLine("UnitOfWork.Dispose");
            context?.Dispose();
        }

        public IRepository<TId, TEntity> GetRepository<TId, TEntity>() where TEntity : class, IIdentityEntity<TId> where TId : struct
        {
            if (repositories.Keys.Contains(typeof(TEntity)))
            {
                return repositories[typeof(TEntity)] as IRepository<TId, TEntity>;
            }

            var repository = new Repository<TId, TEntity>(context);
            repositories.Add(typeof(TEntity), repository);
            return repository;
        }
    }
}
